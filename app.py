# std lib
from os import getenv
from typing import List
from pathlib import Path

# deps
from flask import Flask, g, session, send_file, request, escape
from werkzeug.utils import secure_filename

app = Flask(__name__)
app.secret_key = getenv("JP_SECRET_KEY", "foobar")

data_dir = Path(getenv("JP_DATA_DIR", "./data"))
if not data_dir.exists():
    print(f"mkdir {data_dir}")
    data_dir.mkdir()


def list_pages() -> List[Path]:
    pages: List[Path] = list(data_dir.glob("*"))
    return pages


@app.route("/")
def root():
    pages = list_pages()
    session["visits"] = ((visits := session.get("visits", 0)) + 1) % len(pages)
    return send_file(pages[visits])


@app.route("/admin", methods=("GET", "POST"))
def admin():
    if request.form.get("password") == getenv("JP_PASSWORD", "1234"):
        session["admin"] = True

    if not session.get("admin", False):
        return """
            <form method="POST">
                <input name="password" type="password" placeholder="password"/>
                <input type="submit"/>
            </form>
        """

    if deletefile := request.form.get("deletefile"):
        deletepath = Path(deletefile)
        if not deletepath.exists():
            return f"{deletefile} does not exist", 404
        deletepath.unlink()
        return f"Deleted file {deletefile}"

    if uploadfile := request.files.get("uploadfile"):
        uploadfilename = secure_filename(uploadfile.filename)
        uploadfile.save(data_dir / uploadfilename)
        return f"Uploaded {uploadfilename}"

    def page_entry(p):
        return f"""
            <li>
                {escape(str(p))}
                <form method="POST">
                    <input type="hidden" name="deletefile" value="{escape(p)}"/>
                    <input type="submit" value="delete"/>
                </form>
            </li>
        """

    return f"""
    <form method="POST" enctype="multipart/form-data">
        <input type="file" name="uploadfile"/>
        <input type="submit" value="post that shit"/>
    </form>
    <ul>{"".join(page_entry(p) for p in list_pages())}</ul>
    """

if __name__ == "__main__":
    app.run()
