FROM python:3.9
COPY app.py /app.py
RUN pip3 install flask gunicorn
CMD gunicorn --bind 0.0.0.0:8000 app:app
